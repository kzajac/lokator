package pl.itplusplus.lokator.useCases.payment;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.test.util.ReflectionTestUtils;
import pl.itplusplus.lokator.domain.entities.Apartment;
import pl.itplusplus.lokator.domain.entities.Landlord;
import pl.itplusplus.lokator.useCases.payment.entities.FeeExemption;
import pl.itplusplus.lokator.useCases.payment.entities.FeeExemptionId;
import pl.itplusplus.lokator.useCases.payment.entities.FeeType;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class PaymentsImplTest {

    private Payments payments;

    @BeforeEach
    void initTest() {
        payments = new PaymentsImpl();
        ReflectionTestUtils.setField(payments, "operating", "2");
        ReflectionTestUtils.setField(payments, "repair", "12");
    }

    @Test
    public void landlordShouldBeExemptedFromOperatingInSingleApartment() {
        //given
        Apartment testApartment = new Apartment("1", 10.00);
        FeeExemption feeExemption = new FeeExemption(new FeeExemptionId(FeeType.OPERATING, testApartment), LocalDate.now().plusMonths(3));
        testApartment.setFeeExemptions(Arrays.asList(feeExemption));

        Apartment testApartment2 = new Apartment("2", 15.00);

        Landlord testLandlord = new Landlord();
        testLandlord.setApartments(Arrays.asList(testApartment, testApartment2));

        //then
        payments.calculateLandlordFee(testLandlord);

        //expected
        assertEquals(testLandlord.getOperatingAmount(), BigDecimal.valueOf(30.0));
        assertEquals(testLandlord.getRepairAmount(), BigDecimal.valueOf(300.0));
    }

    @Test
    public void outdatedExemptionsShouldBeIgnored() {
        //given
        Apartment testApartment = new Apartment("1", 10.00);
        FeeExemption feeExemption = new FeeExemption(new FeeExemptionId(FeeType.OPERATING, testApartment), LocalDate.now().minusMonths(3));
        testApartment.setFeeExemptions(Arrays.asList(feeExemption));

        Landlord testLandlord = new Landlord();
        testLandlord.setApartments(Arrays.asList(testApartment));

        //then
        payments.calculateLandlordFee(testLandlord);

        //expected
        assertEquals(testLandlord.getRepairAmount(), BigDecimal.valueOf(120.0));
        assertEquals(testLandlord.getOperatingAmount(), BigDecimal.valueOf(20.0));
    }
}
