package pl.itplusplus.lokator.useCases.payment;

import pl.itplusplus.lokator.domain.entities.BankTransfer;
import pl.itplusplus.lokator.domain.entities.Landlord;
import pl.itplusplus.lokator.domain.entities.Payment;

import java.util.Collection;

public interface PaymentIdentifier {

    Collection<Payment> recognisePayments(Collection<Landlord> allLandlords, Collection<BankTransfer> bankTransfers);

}
