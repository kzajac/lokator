package pl.itplusplus.lokator.useCases.payment.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import pl.itplusplus.lokator.domain.entities.Apartment;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
@Getter
@Setter
@AllArgsConstructor
public class FeeExemptionId  implements Serializable {

    @Enumerated(EnumType.STRING)
    @Column(name = "exemptionFeeType")
    private FeeType exemptionFeeType;

    @ManyToOne
    @JoinColumn(name = "apartment_number")
    @JsonBackReference
    private Apartment apartmentNumber;

    public FeeExemptionId() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FeeExemptionId that = (FeeExemptionId) o;
        return Objects.equals(exemptionFeeType, that.exemptionFeeType) &&
                Objects.equals(apartmentNumber, that.apartmentNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(exemptionFeeType, apartmentNumber);
    }
}
