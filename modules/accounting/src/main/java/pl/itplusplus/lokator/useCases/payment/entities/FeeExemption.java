package pl.itplusplus.lokator.useCases.payment.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import java.time.LocalDate;

@Data
@AllArgsConstructor
@RequiredArgsConstructor
@Entity(name = "feeExemption")
public class FeeExemption {


    @EmbeddedId
    private FeeExemptionId id;

    @Column(name = "expirationDate", nullable = false)
    private LocalDate expirationDate;
}
