package pl.itplusplus.lokator.domain.entities;

import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@Entity
@Table(name="bankTransfer")
@Builder()
@NoArgsConstructor
@AllArgsConstructor
public class BankTransfer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @EqualsAndHashCode.Exclude
    private long bankTransferId;
    @Column(name="srcBankAccount", length = 26, nullable = false)
    private String srcBankAccount;
    @Column(name="dstBankAccount", length = 26, nullable = false)
    private String dstBankAccount;
    @Column(name="transferTitle", length = 300, nullable = false)
    private String transferTitle;
    @Column(name="transferSender", length = 500)
    private String transferSender;
    @Column(name="amount")
    private BigDecimal amount;
    @Column(name="transferDate")
    private LocalDate transferDate;

    public static BankTransferBuilder getBankTransferBuilder() {
        return builder();
    }
}
