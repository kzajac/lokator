package pl.itplusplus.lokator.persistance.repositories;

import org.hibernate.annotations.QueryHints;
import org.springframework.stereotype.Repository;
import pl.itplusplus.lokator.domain.entities.Landlord;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class LandlordRepositoryCustomImpl implements LandlordRepositoryCustom {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Landlord> findAll() {
        List<Landlord> landlords = entityManager.createQuery(
                "select distinct l from Landlord l left join fetch l.payments", Landlord.class)
                .setHint(QueryHints.PASS_DISTINCT_THROUGH, false)
                .getResultList();

        landlords = entityManager.createQuery(
                "select l from Landlord l left join fetch l.payments p left join fetch p.bankTransfer bt " +
                        "where l in :landlords", Landlord.class)

                .setParameter("landlords", landlords)
                .getResultList();

        landlords = entityManager.createQuery(
                "select distinct l from Landlord l left join fetch l.apartments " +
                        "where l in :landlords", Landlord.class)
                .setParameter("landlords", landlords)
                .setHint(QueryHints.PASS_DISTINCT_THROUGH, false)
                .getResultList();

        return landlords;
    }
}
