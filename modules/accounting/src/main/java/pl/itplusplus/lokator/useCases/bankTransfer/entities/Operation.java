package pl.itplusplus.lokator.useCases.bankTransfer.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import pl.itplusplus.lokator.useCases.bankTransfer.utils.JsonDateDeserializer;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Collection;

@JsonDeserialize(builder = Operation.OperationBuilder.class)
public class Operation {

    private String oppositeAcc;
    private String account;
    private Collection<String> details;
    private Collection<String> opposite;
    private LocalDate procDate;
    private BigDecimal amount;

    private Operation(OperationBuilder operation) {
        this.oppositeAcc = operation.oppositeAcc;
        this.account = operation.account;
        this.details = operation.details;
        this.opposite = operation.opposite;
        this.procDate = operation.procDate;
        this.amount = operation.amount;
    }

    public String getOppositeAcc() {
        return oppositeAcc;
    }

    public Collection<String> getDetails() {
        return details;
    }

    public Collection<String> getOpposite() {
        return opposite;
    }

    public LocalDate getProcDate() {
        return procDate;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public String getAccount() {
        return account;
    }

    @JsonPOJOBuilder(withPrefix = "set")
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class OperationBuilder {

        @JacksonXmlProperty(localName = "opposite-acc")
        private String oppositeAcc;
        @JacksonXmlProperty(localName = "acc")
        private String account;
        @JacksonXmlElementWrapper
        @JacksonXmlProperty(localName = "details")
        private Collection<String> details;
        @JacksonXmlElementWrapper
        @JacksonXmlProperty(localName = "opposite")
        private Collection<String> opposite;
        @JsonDeserialize(using = JsonDateDeserializer.class)
        @JacksonXmlProperty(localName = "proc-date")
        private LocalDate procDate;
        @JacksonXmlProperty(localName = "amount")
        private BigDecimal amount;

        public OperationBuilder setOppositeAcc(String oppositeAcc) {
            this.oppositeAcc = oppositeAcc;
            return this;
        }

        public OperationBuilder setDetails(Collection<String> details) {
            this.details = details;
            return this;
        }

        public OperationBuilder setOpposite(Collection<String> opposite) {
            this.opposite = opposite;
            return this;
        }

        public OperationBuilder setProcDate(LocalDate procDate) {
            this.procDate = procDate;
            return this;
        }

        public OperationBuilder setAmount(BigDecimal amount) {
            this.amount = amount;
            return this;
        }

        public OperationBuilder setAccount(String account) {
            this.account = account;
            return this;
        }

        public Operation build() {
            return new Operation(this);
        }
    }
}
