package pl.itplusplus.lokator.useCases;

import pl.itplusplus.lokator.domain.entities.Landlord;

import java.util.Collection;

public interface LandlordService {

    Collection<Landlord> getAllLandlords();
}
