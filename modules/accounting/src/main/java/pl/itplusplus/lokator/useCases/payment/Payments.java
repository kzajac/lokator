package pl.itplusplus.lokator.useCases.payment;

import pl.itplusplus.lokator.domain.entities.BankTransfer;
import pl.itplusplus.lokator.domain.entities.Landlord;

import java.util.Collection;

public interface Payments {

    Collection<BankTransfer> getAllLandlordsPayments(Landlord landlord);

    boolean checkLandlordDept(Landlord landlord);

    Collection<Landlord> getAllBorrowers();

    void calculateLandlordFee(Landlord landlord);
}
