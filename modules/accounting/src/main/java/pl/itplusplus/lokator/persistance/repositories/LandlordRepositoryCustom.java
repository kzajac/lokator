package pl.itplusplus.lokator.persistance.repositories;


import pl.itplusplus.lokator.domain.entities.Landlord;

import java.util.List;

public interface LandlordRepositoryCustom {

    List<Landlord> findAll();
}
