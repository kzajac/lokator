package pl.itplusplus.lokator.useCases.bankTransfer;

import pl.itplusplus.lokator.domain.entities.BankTransfer;
import pl.itplusplus.lokator.useCases.bankTransfer.entities.BankAccHistory;

import java.util.Collection;

public interface BankTransfersImporter {

    Collection<BankTransfer> importBankTransfers(BankAccHistory bankAccHistory);
}
