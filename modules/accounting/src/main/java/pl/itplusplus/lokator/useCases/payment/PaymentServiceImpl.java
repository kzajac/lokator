package pl.itplusplus.lokator.useCases.payment;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.itplusplus.lokator.domain.entities.BankTransfer;
import pl.itplusplus.lokator.domain.entities.Landlord;
import pl.itplusplus.lokator.persistance.repositories.BankTransferRepository;
import pl.itplusplus.lokator.persistance.repositories.LandlordRepository;

import javax.transaction.Transactional;
import java.util.Collection;

@Service
public class PaymentServiceImpl implements PaymentService {

    private LandlordRepository landlordRepository;
    private PaymentIdentifier paymentIdentifier;
    private BankTransferRepository bankTransferRepository;

    @Autowired
    public PaymentServiceImpl(LandlordRepository landlordRepository,
                              PaymentIdentifier paymentIdentifier,
                              BankTransferRepository bankTransferRepository) {
        this.landlordRepository = landlordRepository;
        this.paymentIdentifier = paymentIdentifier;
        this.bankTransferRepository = bankTransferRepository;
    }

    @Override
    @Transactional
    public void loadNewPayments() {
        Collection<Landlord> landlords = landlordRepository.findAll();
        Collection<BankTransfer> bankTransfers = bankTransferRepository.findAllUnidentifiedBankTransfers();
        paymentIdentifier.recognisePayments(landlords, bankTransfers);
    }
}
