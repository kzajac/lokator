package pl.itplusplus.lokator.domain.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Objects;

@Data
@Entity
@Table(name="payment")
@RequiredArgsConstructor
@NoArgsConstructor
public class Payment {

    @Id
    private long id;
    @ManyToOne(fetch = FetchType.LAZY)
    /*@JoinColumn(name = "landlord_landlordId")*/
    @JsonBackReference
    @NonNull  private Landlord landlord;
    @OneToOne(fetch = FetchType.LAZY)
    /*@JoinColumn(name = "bankTransfer_bankTransferId")*/
    @MapsId
    @NonNull private BankTransfer bankTransfer;
    @Column(name = "syncTime", nullable = false)
    @NonNull private LocalDateTime syncTime;

   @Override
    public String toString() {
        return String.format("Payment [id=%d, landlord=(name=%s, surename=%s), amount=%d]",
                this.id, this.landlord.getName(), this.landlord.getSurname(), this.bankTransfer.getAmount());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Payment)) return false;
        Payment that = (Payment) o;
        return this.id != 0L && Objects.equals(this.id, that.id);
    }

    @Override
    public int hashCode() {
        return 31;
    }
}
