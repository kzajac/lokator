package pl.itplusplus.lokator.domain.entities;

import java.time.LocalDateTime;

enum FundType {
    OPERATING,
    REPAIR
}

public class Rent {

    private long fundAmount;
    private FundType fundType;
    private LocalDateTime fundFromDate;
    private LocalDateTime fundToDate;

    public long getFundAmount() {
        return fundAmount;
    }

    public void setFundAmount(long fundAmount) {
        this.fundAmount = fundAmount;
    }

    public FundType getFundType() {
        return fundType;
    }

    public void setFundType(FundType fundType) {
        this.fundType = fundType;
    }

    public LocalDateTime getFundFromDate() {
        return fundFromDate;
    }

    public void setFundFromDate(LocalDateTime fundFromDate) {
        this.fundFromDate = fundFromDate;
    }

    public LocalDateTime getFundToDate() {
        return fundToDate;
    }

    public void setFundToDate(LocalDateTime fundToDate) {
        this.fundToDate = fundToDate;
    }
}
