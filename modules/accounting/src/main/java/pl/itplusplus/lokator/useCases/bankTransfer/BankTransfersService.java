package pl.itplusplus.lokator.useCases.bankTransfer;


import pl.itplusplus.lokator.useCases.bankTransfer.entities.BankAccHistory;

public interface BankTransfersService {

    public void loadNewBankTransfers(BankAccHistory bankAccHistory);

}
