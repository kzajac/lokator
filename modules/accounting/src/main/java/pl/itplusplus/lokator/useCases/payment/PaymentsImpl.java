package pl.itplusplus.lokator.useCases.payment;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import pl.itplusplus.lokator.domain.entities.Apartment;
import pl.itplusplus.lokator.domain.entities.BankTransfer;
import pl.itplusplus.lokator.domain.entities.Landlord;
import pl.itplusplus.lokator.useCases.payment.entities.FeeExemption;
import pl.itplusplus.lokator.useCases.payment.entities.FeeType;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@PropertySource("classpath:businessSettings.properties")
public class PaymentsImpl implements Payments {

    @Value("${accounting.fee.operating}")
    private String operating;

    @Value("${accounting.fee.repair}")
    private String repair;

    @Override
    public Collection<BankTransfer> getAllLandlordsPayments(Landlord landlord) {
        return null;
    }

    @Override
    public boolean checkLandlordDept(Landlord landlord) {
        return false;
    }

    @Override
    public Collection<Landlord> getAllBorrowers() {
        return null;
    }

    @Override
    public void calculateLandlordFee(Landlord landlord) {
        landlord.getApartments()
                .forEach(apartment -> calculateFeeForApartment(landlord, apartment));
    }

    private void calculateFeeForApartment(Landlord landlord, Apartment apartment) {
        Collection<FeeExemption> feeExemptions = Optional.ofNullable(apartment.getFeeExemptions()).orElseGet(Collections::emptyList);
        Collection<FeeExemption> operatingSet = feeExemptions
                .stream()
                .filter(x -> x.getId().getExemptionFeeType() == FeeType.OPERATING && x.getExpirationDate().isAfter(LocalDate.now()))
                .collect(Collectors.toCollection(ArrayList::new));
        Collection<FeeExemption> repairSet = feeExemptions
                .stream()
                .filter(x -> x.getId().getExemptionFeeType() == FeeType.REPAIR && x.getExpirationDate().isAfter(LocalDate.now()))
                .collect(Collectors.toCollection(ArrayList::new));

        if (operatingSet.isEmpty()) {
            double operatingRate = apartment.getSurface() * Double.parseDouble(operating);
            BigDecimal currentOperating = ((landlord.getOperatingAmount() == null) ? BigDecimal.ZERO : landlord.getOperatingAmount());
            landlord.setOperatingAmount(currentOperating.add(BigDecimal.valueOf(operatingRate)));
        } else {
            landlord.setOperatingAmount(BigDecimal.ZERO);
        }
        if (repairSet.isEmpty()) {
            double repairRate = apartment.getSurface() * Double.parseDouble(repair);
            BigDecimal currentRepair = ((landlord.getRepairAmount() == null) ? BigDecimal.ZERO : landlord.getRepairAmount());
            landlord.setRepairAmount(currentRepair.add(BigDecimal.valueOf(repairRate)));
        } else {
            landlord.setRepairAmount(BigDecimal.ZERO);
        }
    }
}
