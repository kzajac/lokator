/*
package pl.itplusplus.lokator.presentation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import pl.itplusplus.lokator.useCases.bankTransfer.BankTransfersService;
import pl.itplusplus.lokator.useCases.payment.PaymentService;

import java.util.TimeZone;

@SpringBootApplication
public class AccountingModuleCliApp implements CommandLineRunner {

    private BankTransfersService bankTransfersService;
    private PaymentService paymentService;

    @Autowired
    public AccountingModuleCliApp(BankTransfersService bankTransfersService, PaymentService paymentService) {
        this.bankTransfersService = bankTransfersService;
        this.paymentService = paymentService;
    }

    public static void main(String[] args) {
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
        SpringApplication.run(AccountingModuleCliApp.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        System.out.println("AAAAAAAAAAAAAA");
        bankTransfersService.loadNewBankTransfers();
        paymentService.loadNewPayments();
    }

}
*/
