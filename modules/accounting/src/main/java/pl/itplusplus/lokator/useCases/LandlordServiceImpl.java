package pl.itplusplus.lokator.useCases;

import org.springframework.stereotype.Service;
import pl.itplusplus.lokator.domain.entities.Landlord;
import pl.itplusplus.lokator.persistance.repositories.LandlordRepository;
import pl.itplusplus.lokator.useCases.payment.Payments;

import java.util.Collection;

@Service
public class LandlordServiceImpl implements  LandlordService {

    private LandlordRepository landlordRepository;
    private Payments payments;

    public LandlordServiceImpl(LandlordRepository landlordRepository, Payments payments) {
        this.landlordRepository = landlordRepository;
        this.payments = payments;
    }

    @Override
    public Collection<Landlord> getAllLandlords() {
        Collection<Landlord> landlords = landlordRepository.findAll();
        landlords.forEach(payments::calculateLandlordFee);
        return landlords;
    }
}
