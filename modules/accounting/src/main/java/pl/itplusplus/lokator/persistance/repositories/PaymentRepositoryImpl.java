package pl.itplusplus.lokator.persistance.repositories;

import org.springframework.stereotype.Repository;
import pl.itplusplus.lokator.domain.entities.Payment;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

@Repository
@Transactional
public class PaymentRepositoryImpl implements PaymentRepository {

    @PersistenceContext
    EntityManager entityManager;

    @Override
    public void savePayment(Payment payment) {
        entityManager.persist(payment);
    }
}
