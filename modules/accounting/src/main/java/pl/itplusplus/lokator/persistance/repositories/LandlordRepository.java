package pl.itplusplus.lokator.persistance.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.itplusplus.lokator.domain.entities.Landlord;

import java.util.Optional;

public interface LandlordRepository extends JpaRepository<Landlord, Long>, LandlordRepositoryCustom {

    Optional<Landlord> findByNameAndSurname(String name, String surname);

    Optional<Landlord> findById(Long id);
}
