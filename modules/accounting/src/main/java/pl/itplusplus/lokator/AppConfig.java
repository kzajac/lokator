package pl.itplusplus.lokator;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {"pl.itplusplus.lokator"})
public class AppConfig {
}
