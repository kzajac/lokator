package pl.itplusplus.lokator.domain.entities;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Objects;

@Data
@Entity
@Table(name="landlord")
@RequiredArgsConstructor
public class Landlord {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long landlordId;
    @Column(name="name", length = 100, nullable = false)
    @NotNull
    private String name;
    @Column(name="surname", length = 100, nullable = false)
    @NotNull private String surname;
    @OneToMany(mappedBy="owner")
    @JsonManagedReference
    private Collection<Apartment> apartments;
    @OneToMany(mappedBy="landlord", cascade = CascadeType.ALL)
    @JsonManagedReference
    private Collection<Payment> payments;
    @Transient
    private BigDecimal operatingAmount;
    @Transient
    private BigDecimal repairAmount;

    public void addPayment(Payment payment) {
        payments.add(payment);
        payment.setLandlord(this);
    }

   @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Landlord)) return false;
        Landlord that = (Landlord) o;
        return this.landlordId != 0L && Objects.equals(this.landlordId, that.landlordId);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return String.format("landlord [name=%s, surname=%s]", this.name, this.surname);
    }
}
