package pl.itplusplus.lokator.useCases.bankTransfer.excptions;

public class BankTransfersImporterException extends  Exception {

    public BankTransfersImporterException(String errorMessage, Throwable err) {
        super(errorMessage, err);
    }

}
