package pl.itplusplus.lokator.persistance.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import pl.itplusplus.lokator.domain.entities.BankTransfer;

import java.util.List;

public interface BankTransferRepository  extends JpaRepository<BankTransfer, Long> {

    List<BankTransfer> findAll();

    @Query(value = "select * from bankTransfer bt " +
            "left join payment p on bt.bankTransferId = p.bankTransfer_bankTransferId " +
            "where bankTransfer_bankTransferId is null and bt.amount > 0",
            nativeQuery = true)
    List<BankTransfer> findAllUnidentifiedBankTransfers();
}
