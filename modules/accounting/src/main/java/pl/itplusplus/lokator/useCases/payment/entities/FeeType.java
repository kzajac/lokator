package pl.itplusplus.lokator.useCases.payment.entities;

public enum FeeType {
    OPERATING,
    REPAIR
}
