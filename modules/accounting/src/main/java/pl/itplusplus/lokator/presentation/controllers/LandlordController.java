package pl.itplusplus.lokator.presentation.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;
import pl.itplusplus.lokator.domain.entities.Landlord;
import pl.itplusplus.lokator.persistance.repositories.LandlordRepository;
import pl.itplusplus.lokator.useCases.LandlordService;

import javax.persistence.EntityExistsException;
import javax.validation.Valid;
import java.util.Collection;
import java.util.Optional;

@RestController
@RequestMapping("/landlord")
public class LandlordController {

    private LandlordRepository landlordRepository;
    private LandlordService landlordService;

    @Autowired
    public LandlordController(LandlordRepository landlordRepository, LandlordService landlordService) {
        this.landlordRepository = landlordRepository;
        this.landlordService = landlordService;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public Collection<Landlord> returnAllLandlords() {
        return landlordService.getAllLandlords();
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> handleAddingNewLandlord(@Valid @RequestBody Landlord newLandlord,
                                                        UriComponentsBuilder builder) {
        Optional<Landlord> landlord = landlordRepository.findByNameAndSurname(newLandlord.getName(), newLandlord.getSurname());
        if (landlord.isPresent()) {
            throw new EntityExistsException("user already exists");
        }
        landlordRepository.save(newLandlord);

        UriComponents uriComponents = builder.path("/landlord/{id}").buildAndExpand(newLandlord.getLandlordId());
        return ResponseEntity.created(uriComponents.toUri()).build();
    }
}
