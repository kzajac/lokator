package pl.itplusplus.lokator.useCases.bankTransfer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.itplusplus.lokator.domain.entities.BankTransfer;
import pl.itplusplus.lokator.persistance.repositories.BankTransferRepository;
import pl.itplusplus.lokator.useCases.bankTransfer.entities.BankAccHistory;

import javax.transaction.Transactional;
import java.util.Collection;

@Service
public class BankTransfersServiceImpl implements BankTransfersService {

    private final BankTransferRepository bankTransferRepository;
    private final BankTransfersImporter bti;

    @Autowired
    public BankTransfersServiceImpl(BankTransferRepository bankTransferRepository,
                                    BankTransfersImporter bti) {
        this.bankTransferRepository = bankTransferRepository;
        this.bti = bti;
    }

    @Override
    @Transactional
    public void loadNewBankTransfers(BankAccHistory bankAccHistory) {
        Collection<BankTransfer> importedBankTransfers = bti.importBankTransfers(bankAccHistory);
        removeDuplicates(importedBankTransfers);
        importedBankTransfers.forEach(bankTransferRepository::save);
    }

    private void removeDuplicates(Collection<BankTransfer> importedBankTransfers) {
        Collection<BankTransfer> existingBankTransfers = bankTransferRepository.findAll();
        importedBankTransfers.removeAll(existingBankTransfers);
    }

}
