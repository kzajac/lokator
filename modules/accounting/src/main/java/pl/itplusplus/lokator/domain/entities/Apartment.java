package pl.itplusplus.lokator.domain.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.lang.NonNull;
import pl.itplusplus.lokator.useCases.payment.entities.FeeExemption;

import javax.persistence.*;
import java.util.Collection;

@Data
@Entity(name = "apartment")
@NoArgsConstructor
@RequiredArgsConstructor
public class Apartment {

    @Id
    @Column(name = "number", length = 10, nullable = false)
    @NonNull private String number;
    @Column(name = "surface", precision = 5, scale = 2)
    @NonNull  private double surface;
    @ManyToOne
    @JoinColumn(name = "landlord_landlordId", nullable = false)
    @JsonBackReference
    private Landlord owner;
    @OneToMany(mappedBy = "id.apartmentNumber", fetch = FetchType.EAGER)
    @JsonManagedReference
    private Collection<FeeExemption> feeExemptions;

}
