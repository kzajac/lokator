package pl.itplusplus.lokator.useCases.bankTransfer;

import org.springframework.stereotype.Service;
import pl.itplusplus.lokator.domain.entities.BankTransfer;
import pl.itplusplus.lokator.useCases.bankTransfer.entities.BankAccHistory;
import pl.itplusplus.lokator.useCases.bankTransfer.entities.Operation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;

@Service
public class BankTransfersImporterXML implements BankTransfersImporter {

    @Override
    public Collection<BankTransfer> importBankTransfers(BankAccHistory bankAccHistory) {
        Collection<BankTransfer> bankTransfers = new ArrayList<>();
        bankAccHistory.getOperations().forEach(x -> bankTransfers.add(mapOperationToBankTransfer(x)));
        removeMalformedBankTransfers(bankTransfers);
        return bankTransfers;
    }

    private void removeMalformedBankTransfers(Collection<BankTransfer> bankTransfers) {
        bankTransfers.removeIf(bt -> (bt.getAmount().compareTo(BigDecimal.ZERO) < 0
                && bt.getSrcBankAccount() == null));
    }

    private BankTransfer mapOperationToBankTransfer(Operation operation) {
        StringBuilder sb = new StringBuilder();
        String transferSender = "";
        if (operation.getOpposite() != null) {
            operation.getOpposite().forEach(sb::append);
            transferSender = sb.toString().toLowerCase();
        }

        sb = new StringBuilder();
        operation.getDetails().forEach(sb::append);
        String transferTitle = sb.toString().toLowerCase();

        BankTransfer bt = BankTransfer.getBankTransferBuilder()
                .amount(operation.getAmount())
                .srcBankAccount(operation.getOppositeAcc())
                .dstBankAccount(operation.getAccount())
                .transferTitle(transferTitle)
                .transferSender(transferSender)
                .transferDate(operation.getProcDate())
                .build();
        return bt;
    }
}
