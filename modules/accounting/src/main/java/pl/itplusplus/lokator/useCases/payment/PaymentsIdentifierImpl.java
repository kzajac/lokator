package pl.itplusplus.lokator.useCases.payment;

import org.springframework.stereotype.Service;
import pl.itplusplus.lokator.domain.entities.BankTransfer;
import pl.itplusplus.lokator.domain.entities.Landlord;
import pl.itplusplus.lokator.domain.entities.Payment;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

@Service
public class PaymentsIdentifierImpl implements PaymentIdentifier {

    @Override
    public Collection<Payment> recognisePayments(Collection<Landlord> allLandlords, Collection<BankTransfer> bankTransfers) {
        Collection<Payment> recognisedPayments = new ArrayList<>();
        for (BankTransfer bankTransfer : bankTransfers) {
            Optional<Landlord> recognisedLandlord = tryToRecognisePayment(bankTransfer, allLandlords);
            if (recognisedLandlord.isPresent()) {
                Landlord landlord = recognisedLandlord.get();
                Payment payment = new Payment(landlord, bankTransfer, LocalDateTime.now());
                recognisedPayments.add(payment);
                landlord.addPayment(payment);
            }
        }
        return recognisedPayments;

    }

    private Optional<Landlord> tryToRecognisePayment(BankTransfer bt, Collection<Landlord> landlords) {
       for (Landlord landlord : landlords) {
            boolean isRepairAmountEqual = landlord.getRepairAmount().equals(bt.getAmount());
            boolean isOperationAmountEqual = landlord.getOperatingAmount().equals(bt.getAmount());
            boolean transferTitle = bt.getTransferSender().toLowerCase().contains(landlord.getName().toLowerCase());
            if ((isRepairAmountEqual || isOperationAmountEqual) && transferTitle) {
                return Optional.of(landlord);
            }
        }
       return Optional.empty();
    }
}
