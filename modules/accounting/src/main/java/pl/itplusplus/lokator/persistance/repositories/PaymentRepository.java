package pl.itplusplus.lokator.persistance.repositories;

import pl.itplusplus.lokator.domain.entities.Payment;

public interface PaymentRepository {

    void savePayment(Payment payment);

}
