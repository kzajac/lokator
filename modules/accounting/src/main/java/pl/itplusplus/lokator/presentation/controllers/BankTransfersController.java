package pl.itplusplus.lokator.presentation.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.itplusplus.lokator.useCases.bankTransfer.BankTransfersService;
import pl.itplusplus.lokator.useCases.bankTransfer.entities.BankAccHistory;

@RestController
@RequestMapping("/bankTransfers")
public class BankTransfersController {

    private final BankTransfersService bankTransfersService;

    @Autowired
    public BankTransfersController(BankTransfersService bankTransfersService) {
        this.bankTransfersService = bankTransfersService;
    }

    @PostMapping(consumes = MediaType.APPLICATION_XML_VALUE)
    public void handleBankTransfersImport(@RequestBody BankAccHistory bankAccHistory) {
        System.out.println(bankAccHistory);
        bankTransfersService.loadNewBankTransfers(bankAccHistory);
    }

}
