-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema lokator
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Table `lokator`.`feeExemption`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `lokator`.`feeExemption` (
  `exemptionFeeType` ENUM('OPERATING', 'REPAIR') NOT NULL,
  `apartment_number` VARCHAR(10) NOT NULL,
  `expirationDate` DATE NOT NULL,
  PRIMARY KEY (`exemptionFeeType`, `apartment_number`),
  INDEX `fk_feeExemption_apartament1_idx` (`apartment_number` ASC),
  CONSTRAINT `fk_feeExemption_apartament1`
    FOREIGN KEY (`apartment_number`)
    REFERENCES `lokator`.`apartment` (`number`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- -------------------------------------------------------------
-- operating and repair fees will me calculated automatically
-- -------------------------------------------------------------
ALTER TABLE `lokator`.`landlord` DROP COLUMN `operatingAmount`;
ALTER TABLE `lokator`.`landlord` DROP COLUMN `repairAmount`;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
