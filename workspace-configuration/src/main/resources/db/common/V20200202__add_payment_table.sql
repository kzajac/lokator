-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema lokator
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema lokator
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Remove foreign key from lokator.bankTransfer because
-- -----------------------------------------------------
ALTER TABLE `lokator`.`bankTransfer` DROP FOREIGN KEY `fk_payment_landlord1`;
ALTER TABLE `lokator`.`bankTransfer` DROP COLUMN `landlord_landlordId`;

CREATE SCHEMA IF NOT EXISTS `lokator` DEFAULT CHARACTER SET latin1 ;
USE `lokator` ;

-- -----------------------------------------------------
-- Table `lokator`.`payment`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `lokator`.`payment` (
  `bankTransfer_bankTransferId` INT(11) NOT NULL,
  `landlord_landlordId` INT(11) NOT NULL,
  `syncTime` DATETIME NOT NULL,
  INDEX `fk_payment_bankTransfer1_idx` (`bankTransfer_bankTransferId` ASC),
  PRIMARY KEY (`bankTransfer_bankTransferId`),
  CONSTRAINT `fk_payment_bankTransfer1`
    FOREIGN KEY (`bankTransfer_bankTransferId`)
    REFERENCES `lokator`.`bankTransfer` (`bankTransferId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_payment_landlord1`
    FOREIGN KEY (`landlord_landlordId`)
    REFERENCES `lokator`.`landlord` (`landlordId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
