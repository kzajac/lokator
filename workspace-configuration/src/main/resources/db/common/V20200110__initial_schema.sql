-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema lokator
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema lokator
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `lokator` DEFAULT CHARACTER SET latin1 ;
USE `lokator` ;

-- -----------------------------------------------------
-- Table `lokator`.`landlord`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `lokator`.`landlord` (
  `landlordId` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NOT NULL,
  `surname` VARCHAR(100) NOT NULL,
  `operatingAmount` DECIMAL(13,2) NULL DEFAULT NULL,
  `repairAmount` DECIMAL(13,2) NULL DEFAULT NULL,
  PRIMARY KEY (`landlordId`))
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `lokator`.`apartment`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `lokator`.`apartment` (
  `number` VARCHAR(10) NOT NULL,
  `surface` DECIMAL(5,2) NOT NULL,
  `landlord_landlordId` INT(11) NOT NULL,
  PRIMARY KEY (`number`),
  INDEX `fk_apartment_landlord_idx` (`landlord_landlordId` ASC),
  CONSTRAINT `fk_apartment_landlord`
    FOREIGN KEY (`landlord_landlordId`)
    REFERENCES `lokator`.`landlord` (`landlordId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `lokator`.`bankTransfer`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `lokator`.`bankTransfer` (
  `bankTransferId` INT(11) NOT NULL AUTO_INCREMENT,
  `srcBankAccount` VARCHAR(26) NOT NULL,
  `dstBankAccount` VARCHAR(26) NOT NULL,
  `transferTitle` VARCHAR(500) NOT NULL,
  `transferDate` DATE NOT NULL,
  `amount` DECIMAL(13,2) NULL DEFAULT NULL,
  `landlord_landlordId` INT(11) NOT NULL,
  PRIMARY KEY (`bankTransferId`),
  INDEX `fk_payment_landlord1_idx` (`landlord_landlordId` ASC),
  CONSTRAINT `fk_payment_landlord1`
    FOREIGN KEY (`landlord_landlordId`)
    REFERENCES `lokator`.`landlord` (`landlordId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
